﻿using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class HeroServerMediator : Mediator {

	[Inject] public HeroServerView View { get; set; }

	public override void OnRegister() {
		Debug.Log(View.checkAfterSeconds);
	}

}
