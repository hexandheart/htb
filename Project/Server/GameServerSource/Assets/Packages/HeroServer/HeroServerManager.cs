﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroServerManager : StrangePackage {

	public override void MapBindings(
			strange.extensions.command.api.ICommandBinder commandBinder,
			strange.extensions.injector.api.ICrossContextInjectionBinder injectionBinder,
			strange.extensions.mediation.api.IMediationBinder mediationBinder) {
		mediationBinder.Bind<HeroServerView>().To<HeroServerMediator>();
	}

	public override void PostBindings(
			strange.extensions.command.api.ICommandBinder commandBinder,
			strange.extensions.injector.api.ICrossContextInjectionBinder injectionBinder,
			strange.extensions.mediation.api.IMediationBinder mediationBinder) {
	}

	public override void Launch(strange.extensions.injector.api.ICrossContextInjectionBinder injectionBinder) {
	}

}
