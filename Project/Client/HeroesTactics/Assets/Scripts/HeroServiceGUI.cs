﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HT;

public class HeroServiceGUI : MonoBehaviour {

	private static readonly int Spacing = 24;

	private enum State {
		Startup,
		Entry,
		ManualLogin,
		Hub,
	}

	[SerializeField] private int offsetX;
	[SerializeField] private int offsetY;

	private string loginId = "test@test.com";
	private string password = "testtest";

	private string deviceId;
	private DeploymentTarget deploymentTarget = DeploymentTarget.Dev;
	private bool interactive = true;

	private State state;

	public void Start() {
		HeroServiceEvents.OnDebugLog += HeroService_OnDebugLog;
		
		HeroServiceEvents.OnInitialized += HeroService_OnInitialized;
		HeroServiceEvents.OnError += HeroService_OnError;

		HeroServiceEvents.OnLogin += HeroService_OnLogin;
		HeroServiceEvents.OnLogout += HeroService_OnLogout;

		if(PlayerPrefs.HasKey("DeviceID")) {
			deviceId = PlayerPrefs.GetString("DeviceID");
		}
		else {
			deviceId = System.Guid.NewGuid().ToString();
			PlayerPrefs.SetString("DeviceID", deviceId);
			PlayerPrefs.Save();
		}

		state = State.Startup;
	}

	public void OnGUI() {
		GUI.enabled = true;

		int xpos = 10 + offsetX;
		int ypos = 10 + offsetY;

		GUI.Label(new Rect(xpos, ypos, 400, 20), "Status: " + HeroService.GetStatusString());
		ypos += Spacing;
		GUI.Label(new Rect(xpos, ypos, 400, 20), "Device ID: " + deviceId);
		GUI.enabled = !HeroService.IsInitialized;
		if(GUI.Button(new Rect(xpos + 400, ypos, 200, 20), "Reset")) {
			deviceId = System.Guid.NewGuid().ToString();
			PlayerPrefs.SetString("DeviceID", deviceId);
			PlayerPrefs.Save();
		}
		ypos += Spacing * 2;

		GUI.enabled = interactive;

		switch(state) {
		case State.Startup:
			ypos = OnGUI_Startup(ypos);
			break;
		case State.Entry:
			ypos = OnGUI_Entry(ypos);
			break;
		case State.ManualLogin:
			ypos = OnGUI_ManualLogin(ypos);
			break;
		case State.Hub:
			ypos = OnGUI_Hub(ypos);
			break;
		}
	}

	private int OnGUI_Startup(int ypos) {
		int xpos = 10 + offsetX;
		
		bool useLiveTarget = deploymentTarget == DeploymentTarget.Test;
		useLiveTarget = GUI.Toggle(new Rect(xpos, ypos, 200, 20), useLiveTarget, useLiveTarget ? "Test" : "Dev");
		deploymentTarget = useLiveTarget ? DeploymentTarget.Test : DeploymentTarget.Dev;
		ypos += Spacing;

		if(GUI.Button(new Rect(xpos, ypos, 200, 20), "Initialize")) {
			interactive = false;
			HeroService.Initialize(deploymentTarget);
		}
		ypos += Spacing;

		return ypos;
	}

	private int OnGUI_Entry(int ypos) {
		int xpos = 10 + offsetX;

		GUI.Label(new Rect(xpos, ypos, 200, 20), "Choose login method");
		ypos += Spacing;

		if(GUI.Button(new Rect(xpos, ypos, 200, 20), "Play now!")) {
			// Attempt auto login.
			interactive = false;
			HeroService.Authentication.AutoLogin(deviceId);
		}
		ypos += Spacing;

		if(GUI.Button(new Rect(xpos, ypos, 200, 20), "Manual login")) {
			// Go to manual login.
			state = State.ManualLogin;
		}
		ypos += Spacing;

		ypos += Spacing;
		return ypos;
	}

	private int OnGUI_ManualLogin(int ypos) {
		int xpos = 10 + offsetX;

		GUI.Label(new Rect(xpos, ypos, 200, 20), "Not supported yet");
		ypos += Spacing;
		
		if(GUI.Button(new Rect(xpos, ypos, 200, 20), "Back")) {
			state = State.Entry;
		}
		ypos += Spacing;

		/*GUILayout.BeginArea(new Rect(xpos, ypos, 400, Spacing * 3));

		GUI.Label(new Rect(0, Spacing * 0, 200, 20), "ID");
		loginId = GUI.TextField(new Rect(200, Spacing * 0, 200, 20), loginId);

		GUI.Label(new Rect(0, Spacing * 1, 200, 20), "Password");
		password = GUI.PasswordField(new Rect(200, Spacing * 1, 200, 20), password, '*');

		if(GUI.Button(new Rect(0, Spacing * 2, 200, 20), "Log in")) {
			interactive = false;
			HeroService.Authentication.Login(loginId, password);
		}
		GUILayout.EndArea();
		ypos += Spacing * 3;*/
		
		ypos += Spacing;
		return ypos;
	}

	private int OnGUI_Hub(int ypos) {
		int xpos = 10 + offsetX;

		GUI.Label(new Rect(xpos, ypos, 200, 20), "HUB");
		ypos += Spacing;
		
		if(GUI.Button(new Rect(xpos, ypos, 200, 20), "Log out")) {
			interactive = false;
			HeroService.Authentication.Logout();
		}
		ypos += Spacing;
		
		ypos += Spacing;
		return ypos;
	}

	private void HeroService_OnDebugLog(string message) {
		Debug.Log("[HS]: " + message);
	}

	private void HeroService_OnInitialized(DeploymentTarget deploymentTarget) {
		Debug.LogFormat("Hero service initialized: {0}", deploymentTarget);
		state = State.Entry;
		interactive = true;
	}

	private void HeroService_OnLogin() {
		Debug.Log("Hero service logged in");
		state = State.Hub;
		interactive = true;
	}

	private void HeroService_OnLogout() {
		Debug.Log("Hero service logged out");
		state = State.Entry;
		interactive = true;
	}

	private void HeroService_OnError(int errorCode, string errorMessage) {
		Debug.LogErrorFormat("Hero service error {0}: {1}", errorCode, errorMessage);
		RefreshState();
		interactive = true;
	}

	private void RefreshState() {
		if(!HeroService.IsInitialized) {
			state = State.Startup;
		}
		else {
			if(HeroService.Authentication.IsLoggedIn) {
				state = State.Hub;
			}
			else {
				state = State.Entry;
			}
		}
	}

}
