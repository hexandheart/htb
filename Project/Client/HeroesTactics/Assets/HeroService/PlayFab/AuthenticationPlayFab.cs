﻿using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;

namespace HT {

	public class AuthenticationPlayFab : Authentication {

		public override void Initialize(DeploymentTarget deploymentTarget) {
			DeploymentTarget = deploymentTarget;
			PlayFabSettings.TitleId = TitleIDs[(int)DeploymentTarget];
		}

		public override bool IsLoggedIn {
			get {
				return !forceLogout && PlayFabClientAPI.IsClientLoggedIn();
			}
		}

		public override void AutoLogin(string deviceId) {
			var request = new LoginWithCustomIDRequest() {
				CustomId = deviceId,
				CreateAccount = true,
				TitleId = TitleIDs[(int)DeploymentTarget],
				InfoRequestParameters = new GetPlayerCombinedInfoRequestParams() {
					GetTitleData = true,
				},
			};
			forceLogout = false;
			PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnAutoLoginError);
		}

		public override void Login(string id, string password) {
			var request = new LoginWithEmailAddressRequest() {
				Email = id,
				Password = password,
				TitleId = TitleIDs[(int)DeploymentTarget],
				InfoRequestParameters = new GetPlayerCombinedInfoRequestParams() {
					GetTitleData = true,
				},
			};
			forceLogout = false;
			PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginError);
		}

		public override void Logout() {
			/*HeroServiceEvents.TriggerError(400, "Logout not supported");
			throw new System.NotImplementedException();*/
			forceLogout = true;
			HeroServiceEvents.TriggerLogout();
		}

		/***************************************************************************************/
		#region PlayFab event callbacks

		private void OnLoginSuccess(LoginResult result) {
			if(result.InfoResultPayload != null && result.InfoResultPayload.TitleData != null) {
				string message = "Title data:\n";
				foreach(var kv in result.InfoResultPayload.TitleData) {
					message += string.Format("{0}: {1}\n", kv.Key, kv.Value);
				}
				HeroServiceEvents.TriggerDebugLog(message);
			}
			HeroServiceEvents.TriggerLogin();
		}

		private void OnAutoLoginError(PlayFabError error) {
			if(error.Error == PlayFabErrorCode.AccountNotFound) {
			}
			HeroServiceEvents.TriggerError(error.HttpCode, error.GenerateErrorReport());
		}

		private void OnLoginError(PlayFabError error) {
			HeroServiceEvents.TriggerError(error.HttpCode, error.GenerateErrorReport());
		}

		private void OnError(PlayFabError error) {
			HeroServiceEvents.TriggerError(error.HttpCode, error.GenerateErrorReport());
		}

		#endregion //PlayFab event callbacks

		/***************************************************************************************/
		#region Deployment configuration

		private static readonly string[] TitleIDs = {
			"EC8A", // Dev
			"F22E", // Test
		};

		private bool forceLogout = false;

		#endregion

	}

}
