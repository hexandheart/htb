﻿namespace HT {

	public abstract class Authentication {

		public DeploymentTarget DeploymentTarget { get; protected set; }

		public abstract void Initialize(DeploymentTarget deploymentTarget);

		public abstract bool IsLoggedIn { get; }
		public abstract void AutoLogin(string deviceId);
		public abstract void Login(string id, string password);
		public abstract void Logout();

		/***************************************************************************************/
		#region Privates

		#endregion

	}

}
