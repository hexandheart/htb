﻿namespace HT {

	public static class HeroServiceEvents  {

		public delegate void DebugLogHandler(string message);
		public static DebugLogHandler OnDebugLog;
		public static void TriggerDebugLog(string message) {
			if(OnDebugLog != null) {
				OnDebugLog(message);
			}
		}

		public delegate void ErrorHandler(int errorCode, string errorMessage);
		public static ErrorHandler OnError;
		public static void TriggerError(int errorCode, string errorMessage) {
			if(OnError != null) {
				OnError(errorCode, errorMessage);
			}
		}

		public delegate void InitializedHandler(DeploymentTarget deploymentTarget);
		public static InitializedHandler OnInitialized;
		public static void TriggerInitialized(DeploymentTarget deploymentTarget) {
			if(OnInitialized != null) {
				OnInitialized(deploymentTarget);
			}
		}

		public delegate void LoginHandler();
		public static LoginHandler OnLogin;
		public static void TriggerLogin() {
			if(OnLogin != null) {
				OnLogin();
			}
		}

		public delegate void LogoutHandler();
		public static LogoutHandler OnLogout;
		public static void TriggerLogout() {
			if(OnLogout != null) {
				OnLogout();
			}
		}

	}

}
