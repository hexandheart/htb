﻿namespace HT {

	public enum DeploymentTarget {
		Dev,
		Test,
	}

	public static class HeroService {

		public static string GetStatusString() {
			if(!IsInitialized) {
				return "Not initialized";
			}

			if(Authentication.IsLoggedIn) {
				return string.Format("Logged in [{0}]", DeploymentTarget);
			}

			return string.Format("Initialized [{0}]", DeploymentTarget);
		}

		public static bool IsInitialized {
			get {
				return Authentication != null;
			}
		}

		public static void Initialize(DeploymentTarget deploymentTarget) {
			DeploymentTarget = deploymentTarget;

			// Initialize the platform-appropriate services?
			Authentication = new AuthenticationPlayFab();
			Authentication.Initialize(deploymentTarget);

			HeroServiceEvents.TriggerInitialized(deploymentTarget);
		}

		public static DeploymentTarget DeploymentTarget { get; private set; }
		public static Authentication Authentication { get; private set; }

		/***************************************************************************************/
		#region Privates

		#endregion

	}

}
